module Delude 
(
) where

import Data.Constraint
import Data.Kind
import GHC.Types
import GHC.Integer




class Category (arrow :: obj -> obj -> Type) where
  type Valid arrow (a :: obj) :: Constraint
  witness :: arrow a b -> Dict (Valid arrow a, Valid arrow b)
  id  :: Valid arrow a => arrow a a
  (.) :: arrow b c -> arrow a b -> arrow a c

class (Category c, Category d)
  => CovariantFunctor c d f where
  co :: c a b -> d (f a) (f b)

class (Category c, Category d)
  => ContravariantFunctor c d f where
  contra :: c a b -> d (f b) (f a)

-- | Semigroups are weaker than Monoids,
-- its just an associative operation.
class Semigroup m where
  (<>) :: m -> m -> m

instance Semigroup [a] where
  []     <> b = b
  (a:as) <> b = a : (as <> b)

-- | Monoids are a useful abstraction
-- empty ++ a = a = a ++ empty forall a,
-- (++) associative
class Monoid m where
  empty :: m
  (++)  :: m -> m -> m

instance Monoid [a] where
  (++) = (<>)
  empty = []


-- | Hask Category
-- ====================
-- Free functions over 
-- Haskell data types.
instance Category (->) where
  type Valid (->) a = ()
  witness _ = Dict
  id    x = x
  (f.g) x = f (g x)

-- | (a ->) is a covariant functor from Hask to Hask,
-- with composition as the mapping.
instance CovariantFunctor (->) (->) ((->) a) where
  co = (.)
